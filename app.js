var express = require('express');
var multer  = require('multer');

var upload = multer({ dest: 'uploads/' });

var sloRouter = require('./routes/sloRoute');
var unitRouter = require('./routes/unitRoute');
var studentRouter = require('./routes/studentRoute');
var downloadRouter = require('./routes/downloadRoute');
var imageRouter = require('./routes/imageRoute');

var app = express();

app.use('/slo', sloRouter);
app.use('/unit', unitRouter);
app.use('/student', studentRouter);
app.use('/public',downloadRouter);
app.use('/images',imageRouter);
app.use('/static',express.static('static'));



app.listen(3300,function () {
   console.log("Listening at port 3300...");
});