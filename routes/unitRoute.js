var colors = require('colors');
var express = require('express');
var router = express.Router();

var fs = require('fs');

var multer  = require('multer');
var upload = multer({ dest: 'temp/' });

var unitManager = require('./../api-v1/unitFileManager');

router.post('/upload/material',upload.single('file'), function (req, res, next) {
    var log = '\n\nCoordinator SLO-Practice Upload { \n\tFile Name: ' + req.file.originalname + "\n\tFile Size: " +
        req.file.size + "\n\tUNIT ID: " + req.body.unitID + "\n}";
    console.log(log.green);
    unitManager.saveFile(req.file.originalname,req.file.filename,req.body.unitID,req.body.courseID,function() {
        res.send("Ünder Development");
    });
});


router.post('/list/material',upload.array(), function (req, res, next) {
    unitManager.listFile(req.body.unitID,req.body.courseID,function (fileList) {
        res.send(fileList);
    });
});


router.post('/delete/material',upload.array(), function (req, res, next) {
    unitManager.deleteFile(req.body.unitID,req.body.courseID,req.body.fileName,function (msg) {
        res.send(msg);
    });
});



module.exports = router;
