var aes256 = require('aes256');

var key = 'ulcKeyWkefwfjiwfowfio';

var encrypt = function (plaintext) {
    return aes256.encrypt(key,plaintext)
};

var decrypt = function (ciphertext) {
    return aes256.decrypt(key,ciphertext);
};

module.exports.encrypt = encrypt;
module.exports.decrypt = decrypt;