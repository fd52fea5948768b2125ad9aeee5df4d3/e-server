var fs = require('fs');
var path = require('path');
var cryptor = require('./../api-v1/encryptor');

function makeDir(path) {
    if (!fs.existsSync(path)){
        fs.mkdirSync(path);
    }
}

var saveFile = function (files,sessionID,courseID,quizID,types,callback) {
    makeDir('./data');
    makeDir('./data/images');
    makeDir('./data/images/' + types);
    makeDir('./data/images/'+ types +"/"+ courseID);
    makeDir('./data/images/'+ types +"/"+ courseID + "/" + sessionID);
    makeDir('./data/images/'+ types +"/"+ courseID + "/" + sessionID + "/" + quizID);
    var qDir = './data/images/'+ types +"/"+ courseID + "/" + sessionID + "/" + quizID + '/' + 'question';
    makeDir(qDir);
    var aDir = './data/images/'+ types +"/"+ courseID + "/" + sessionID + "/" + quizID + '/' + 'answer';
    makeDir(aDir);



    var i = 1;
    var length = files.length;
    files.forEach(function (myFile) {
        if(myFile.fieldname.substr(0,1) == 'a') {
            var extension = myFile.originalname.split('.').pop().toLowerCase();
            fs.createReadStream('./temp/'+myFile.filename).pipe(fs.createWriteStream(aDir + "/file" + i + "."+extension ));
            fs.unlink('./temp/'+myFile.filename,function (err) {
                if(err) {
                    console.error("Error in deleting temp file [" + myFile.filename +"]: " + err.message);
                    callback("Something wrong");
                    return;
                }
            });
        }else {
            var extension = myFile.originalname.split('.').pop().toLowerCase();
            fs.createReadStream('./temp/'+myFile.filename).pipe(fs.createWriteStream(qDir + "/file" + i + "."+extension ));
            fs.unlink('./temp/'+myFile.filename,function (err) {
                if(err) {
                    console.error("Error in deleting temp file [" + myFile.filename +"]: " + err.message);
                    callback("Something wrong");
                    return;
                }
            });
        }
        if(i == files.length) {
            callback("Done");
        }
        i++;
    });
};


var listFile = function (courseID,sessionID,quizID,type,callback) {
    var dir = './data/images/' +type+"/"+ courseID + "/"+ sessionID +"/"+ quizID + "/question/";
    //console.log(dir);
    if (fs.existsSync(dir)) {
        var fileList = [];
        fs.readdir(dir, function (err, files) {
            files.forEach(function (file) {
                var temp = {
                    'name': file,
                    'path': encodeURIComponent(cryptor.encrypt(path.resolve('data','images',type,courseID,sessionID,quizID,'question',file))),
                    'type': 'question'
                };
                fileList.push(temp);
            });
            var dir = './data/images/' +type+"/"+ courseID + "/"+ sessionID +"/"+ quizID + "/answer";
            console.log(dir);
            if (fs.existsSync(dir)) {
                fs.readdir(dir, function (err, files) {
                    files.forEach(function (file) {
                        var temp = {
                            'name': file,
                            'path': encodeURIComponent(cryptor.encrypt(path.resolve('data','images',type,courseID,sessionID,quizID,'answer',file))),
                            'type': 'answer'
                        };
                        fileList.push(temp);
                    });
                    console.log(fileList);
                    callback(JSON.stringify(fileList));
                });
            }
        });
    }else {
        callback("[]");
    }


};

function rmdir(d) {
    var self = arguments.callee
    if (fs.existsSync(d)) {
        fs.readdirSync(d).forEach(function(file) {
            var C = d + '/' + file
            if (fs.statSync(C).isDirectory()) self(C)
            else fs.unlinkSync(C)
        })
        fs.rmdirSync(d)
    }
}

var deleteFile = function (courseID,sessionID,quizId,types,callback) {
    var dirPath = './data/images/'+"/"+types +"/"+ courseID + "/"+sessionID+"/" + quizId+ "/";
    console.log("Removing dir -> " + dirPath);
    rmdir(dirPath);
    callback("File deleted");

};



module.exports.saveFile = saveFile;
module.exports.listFile = listFile;
module.exports.deleteFile = deleteFile;