var colors = require('colors');
var express = require('express');
var router = express.Router();
var crypt = require("./../api-v1/encryptor");

var fs = require('fs');
var path = require('path');

var multer  = require('multer');
var crypto = require("crypto");
var upload = multer({ dest: 'temp/' });


router.get('/',upload.array(), function (req, res, next) {
    if(!req.query.q) {
        res.sendStatus(404);
    }else {
        var dir = path.resolve(crypt.decrypt(req.query.q));

        if(fs.existsSync(dir)) {
            var data =fs.readFileSync(dir);
            res.contentType("application/pdf");
            res.send(data);
        }else {
            res.sendStatus(404);
        }
    }
});




module.exports = router;
