var fs = require('fs');
var path = require('path');
var cryptor = require('./../api-v1/encryptor');

function makeDir(path) {
    if (!fs.existsSync(path)){
        fs.mkdirSync(path);
    }
}

var saveFile = function (filename,savedName,unitID,courseID,callback) {
    makeDir('./data');
    makeDir('./data/coordinator');
    makeDir('./data/coordinator/' + courseID);
    makeDir('./data/coordinator/' + courseID + "/" + 'unit');
    var dir = './data/coordinator/' + courseID + "/unit/" + unitID;
    makeDir(dir);

    var files = fs.readdirSync(dir);
    if(files.length > 1)  {
        callback("Error more that one file already present");
        return;
    }

    fs.createReadStream('./temp/'+savedName).pipe(fs.createWriteStream(dir + "/" + filename));
    fs.unlink('./temp/'+savedName,function (err) {
        if(err) {
            console.error("Error in deleting temp file [" + savedName +"]: " + err.message);
            callback("Something wrong");
            return;
        }
        callback("Success");
    });

};


var listFile = function (unitID,courseID,callback) {
    var dir = './data/coordinator/' + courseID + "/unit/"+ unitID;
    if (fs.existsSync(dir)) {
        var fileList = [];
        fs.readdir(dir, function (err, files) {
            files.forEach(function (file) {
                var temp = {
                    'name': file,
                    'path': encodeURIComponent(cryptor.encrypt(path.resolve('data','coordinator',courseID,'unit',unitID,file)))
                };
                fileList.push(temp);
            });
            callback(JSON.stringify(fileList));
        });
    }else {
        callback("[]");
    }
};

var deleteFile = function (unitID,courseID,filename,callback) {
    var dir = './data/coordinator/' + courseID + "/"+'unit'+"/" + unitID + "/"+filename;
    if (fs.existsSync(dir)) {
        fs.unlinkSync(dir);
    }
    callback("File deleted");

};



module.exports.saveFile = saveFile;
module.exports.listFile = listFile;
module.exports.deleteFile = deleteFile;