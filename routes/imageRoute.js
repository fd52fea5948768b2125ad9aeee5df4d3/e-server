var colors = require('colors');
var express = require('express');
var router = express.Router();

var fs = require('fs');

var multer  = require('multer');
var upload = multer({ dest: 'temp/' });

var imageManager = require('./../api-v1/imageManager');

//QUIZ
router.post('/upload/quiz',upload.any(), function (req, res, next) {
    if(!req.files || req.files.length == 0)  {
        res.send("Done");
        return;
    }
    imageManager.saveFile(req.files,req.body.sessionID,req.body.courseID,req.body.qID,'quiz',function () {
        res.send("Done");
    });

});

router.post('/list/quiz',upload.array(), function (req, res, next) {
    console.log("QUiz Log Requested");
    imageManager.listFile(req.body.courseID,req.body.sessionID,req.body.qID,'quiz',function (data) {
        res.send(data);
        console.log(data);
    });
});


router.post('/delete/quiz',upload.array(), function (req, res, next) {
    imageManager.deleteFile(req.body.courseID,req.body.sessionID,req.body.qID,'quiz',function () {
        res.send("Deleted");
    });
});

//SHORT ANSWER
router.post('/upload/sa',upload.any(), function (req, res, next) {
    if(!req.files || req.files.length == 0)  {
        res.send("Done");
        return;
    }
    imageManager.saveFile(req.files,req.body.sessionID,req.body.courseID,req.body.qID,'sa',function () {
        res.send("Done");
    });

});

router.post('/list/sa',upload.array(), function (req, res, next) {
    imageManager.listFile(req.body.courseID,req.body.sessionID,req.body.qID,'sa',function (data) {
        res.send(data);
    });
});


router.post('/delete/sa',upload.array(), function (req, res, next) {
    imageManager.deleteFile(req.body.courseID,req.body.sessionID,req.body.qID,'sa',function () {
        res.send("Deleted");
    });
});

//LONG ANSWER
router.post('/upload/la',upload.any(), function (req, res, next) {
    if(!req.files || req.files.length == 0)  {
        res.send("Done");
        return;
    }
    imageManager.saveFile(req.files,req.body.sessionID,req.body.courseID,req.body.qID,'la',function () {
        res.send("Done");
    });

});

router.post('/list/la',upload.array(), function (req, res, next) {
    imageManager.listFile(req.body.courseID,req.body.sessionID,req.body.qID,'la',function (data) {
        res.send(data);
    });
});


router.post('/delete/la',upload.array(), function (req, res, next) {
    imageManager.deleteFile(req.body.courseID,req.body.sessionID,req.body.qID,'la',function () {
        res.send("Deleted");
    });
});

module.exports = router;
