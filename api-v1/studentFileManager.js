var fs = require('fs');
var path = require('path');
var cryptor = require('./../api-v1/encryptor');

function makeDir(path) {
    if (!fs.existsSync(path)){
        fs.mkdirSync(path);
    }
}

var saveFile = function (filename,savedName,sloID,courseID,studentID,callback) {
    makeDir('./data');
    makeDir('./data/student');
    makeDir('./data/student/'+studentID);
    makeDir('./data/student/'+studentID+"/" + courseID);
    var dir = './data/student/'+studentID+"/" + courseID + "/"+sloID;
    makeDir(dir);


    fs.createReadStream('./temp/'+savedName).pipe(fs.createWriteStream(dir + "/" + filename));
    fs.unlink('./temp/'+savedName,function (err) {
        if(err) {
            console.error("Error in deleting temp file [" + savedName +"]: " + err.message);
            callback("Something wrong");
            return;
        }
        callback({
            'url': encodeURIComponent(cryptor.encrypt(path.resolve('data','student',studentID,courseID,sloID,filename)))
        });
    });

};

module.exports.saveFile = saveFile;
