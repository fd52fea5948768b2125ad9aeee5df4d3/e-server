var colors = require('colors');
var express = require('express');
var router = express.Router();

var fs = require('fs');

var multer  = require('multer');
var upload = multer({ dest: 'temp/' });

var studentManager = require('./../api-v1/studentFileManager');

router.post('/upload/',upload.single('file'), function (req, res, next) {
    var log = '\n\nStudent SLO-Practice Upload { \n\tFile Name: ' + req.file.originalname + "\n\tFile Size: " +
        req.file.size + "\n\tSLO ID: " + req.body.sessionID + "\n}";
    console.log(log.green);
    studentManager.saveFile(req.file.originalname,req.file.filename,req.body.sessionID,req.body.courseID,req.body.studentID,function(data) {
        console.log(data);
        res.send(data);
});
});



module.exports = router;
