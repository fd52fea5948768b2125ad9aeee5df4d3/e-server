var colors = require('colors');
var express = require('express');
var router = express.Router();

var fs = require('fs');

var multer  = require('multer');
var upload = multer({ dest: 'temp/' });

var sloManager = require('./../api-v1/sloFileManager');

router.post('/upload/practice',upload.single('file'), function (req, res, next) {
    var log = '\n\nCoordinator SLO-Practice Upload { \n\tFile Name: ' + req.file.originalname + "\n\tFile Size: " +
        req.file.size + "\n\tSLO ID: " + req.body.sessionID + "\n}";
    console.log(log.green);
    sloManager.saveFile(req.file.originalname,req.file.filename,req.body.sessionID,req.body.courseID,"slp",function() {
        res.send("Ünder Development");
    });
});

router.post('/upload/content', upload.single('file'), function (req, res, next) {
    var log = '\n\nCoordinator SLO-Content Upload { \n\tFile Name: ' + req.file.originalname + "\n\tFile Size: " +
        req.file.size + "\n\tSLO ID: " + req.body.sessionID + "\n}";
    console.log(log.green);
    sloManager.saveFile(req.file.originalname,req.file.filename,req.body.sessionID,req.body.courseID,"slc",function () {
        res.send("Ünder Development");
    });
});


router.post('/list/practice',upload.array(), function (req, res, next) {
    sloManager.listFile(req.body.sessionID,req.body.courseID,"slp",function (fileList) {
        res.send(fileList);
    });
});

router.post('/list/content',upload.array(), function (req, res, next) {
    sloManager.listFile(req.body.sessionID,req.body.courseID,"slc",function (fileList) {
        res.send(fileList);
    });
});



router.post('/delete/content',upload.array(), function (req, res, next) {
    sloManager.deleteFile(req.body.sessionID,req.body.courseID,req.body.fileName,"slc",function (msg) {
        res.send(msg);
    });
});

router.post('/delete/practice',upload.array(), function (req, res, next) {
    sloManager.deleteFile(req.body.sessionID,req.body.courseID,req.body.fileName,"slp",function (msg) {
        res.send(msg);
    });
});


module.exports = router;
